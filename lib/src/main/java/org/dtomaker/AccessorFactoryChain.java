package org.dtomaker;

import java.util.function.Function;

public class AccessorFactoryChain extends AbstractAccessorFactory {

	private static class AccessorFactoryChainNode {

		private AbstractAccessorFactory factory;
		private AccessorFactoryChainNode next;

		public AccessorFactoryChainNode(AbstractAccessorFactory factory, AccessorFactoryChainNode next) {
			this.factory = factory;
			this.next = next;
		}

		public <R> R chainCall(Function<? super AbstractAccessorFactory, R> call) {
			R res = call.apply(factory);
			if(null != res)
				return res;

			return next.chainCall(call);
		}
	}

	private AccessorFactoryChainNode head;

	public AccessorFactoryChain() {
		head = new AccessorFactoryChainNode(null, null) { // guard node

			@Override
			public <R> R chainCall(Function<? super AbstractAccessorFactory, R> call) {
				return null;
			}
		};
	}

	@Override
	public Getter createGetter(Class<?> clazz, String field) {
		return head.chainCall(f -> f.createGetter(clazz, field));
	}

	@Override
	public Setter createSetter(Class<?> clazz, String field) {
		return head.chainCall(f -> f.createSetter(clazz, field));
	}

	public void registerAccessorFactory(AbstractAccessorFactory factory) {
		head = new AccessorFactoryChainNode(factory, head);
	}
}
