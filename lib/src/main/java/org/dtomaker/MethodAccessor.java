package org.dtomaker;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodAccessor {

	private Method method;

	public MethodAccessor(Method m) {
		method = m;
	}

	protected Object invoke(Object target, Object... args) {
		try {
			return method.invoke(target, args);
		} catch(InvocationTargetException ex) {
			throw new RuntimeException(
					String.format("Setter [%s] failed with exception: %s", method, ex.getMessage()),
					ex);
		} catch(IllegalAccessException ex) {
			throw new RuntimeException(
					String.format("Setter [%s] is not accessible: %s", method, ex.getMessage()),
					ex);
		}
	}
}
