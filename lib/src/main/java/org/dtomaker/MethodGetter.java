package org.dtomaker;

import java.lang.reflect.Method;

public class MethodGetter extends MethodAccessor implements Getter {

	public MethodGetter(Method m) {
		super(m);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(Object source) throws IllegalAccessException {
		return (T) invoke(source);
	}
}
