package org.dtomaker;

public interface Setter {

	void set(Object target, Object val) throws IllegalAccessException;
}
