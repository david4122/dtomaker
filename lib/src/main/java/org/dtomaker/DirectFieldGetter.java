package org.dtomaker;

import java.lang.reflect.Field;

public class DirectFieldGetter implements Getter {

	private Field field;

	public DirectFieldGetter(Field f) {
		field = f;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(Object source) throws IllegalAccessException {
		return (T) field.get(source);
	}
}
