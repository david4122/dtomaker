package org.dtomaker;

import java.lang.reflect.Field;

public class DirectFieldSetter implements Setter {

	private Field field;

	public DirectFieldSetter(Field f) {
		field = f;
	}

	@Override
	public void set(Object target, Object val) throws IllegalAccessException {
		field.set(target, val);
	}
}
