package org.dtomaker;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.dtomaker.annotation.From;
import org.dtomaker.annotation.Ignore;

public class DtoMaker {

	private AbstractAccessorFactory accessorFactory;

	public DtoMaker(AbstractAccessorFactory accessorFactory) {
		this.accessorFactory = accessorFactory;
	}

	public <S, T> T toDto(S source, Class<T> dtoClazz) {
		var dto = newInstance(dtoClazz);
		copyInto(source, dto);

		return dto;
	}

	public <S, T> void copyInto(S source, T target) {
		try {
			for(Field f: target.getClass().getDeclaredFields()) {
				if(f.getAnnotation(Ignore.class) != null)
					continue;

				Getter getter = accessorFactory.createGetter(source.getClass(), getSourceFieldName(f));

				if(null == getter)
					throw new IllegalArgumentException(String.format(
								"Could not find getter for [%s] in [%s]",
								f.getName(), source.getClass()));

				Setter setter = accessorFactory.createSetter(target.getClass(), f.getName());

				if(null == setter)
					throw new IllegalArgumentException(String.format(
								"Could not find setter for [%s] in [%s]",
								f.getName(), source.getClass()));

				setter.set(target, getter.get(source));
			}
		} catch(IllegalAccessException ex) {
			throw new RuntimeException(ex);
		}
	}

	private String getSourceFieldName(Field target) {
		From meta = target.getAnnotation(From.class);
		if(null == meta)
			return target.getName();

		return meta.value();
	}

	private <T> T newInstance(Class<T> clazz) {
		try {
			var constructor = clazz.getDeclaredConstructor();

			try {

				return constructor.newInstance();

			} catch(InvocationTargetException ex) {
				throw new RuntimeException(
						String.format("Constructor [%s] threw an exception: %s", constructor, ex.getMessage()),
						ex);
			}
		} catch(NoSuchMethodException ex) {
			throw new IllegalArgumentException(
					String.format("Class [%s] has to define a no-arg constructor", clazz.getName()),
					ex);
		} catch(IllegalAccessException ex) {
			throw new IllegalArgumentException(
					String.format("Could not access the no-arg constructor of [%s]: %s", clazz, ex.getMessage()),
					ex);
		} catch(InstantiationException ex) {
			throw new IllegalArgumentException(
					String.format("Cannot instantiate abstract class [%s]: %s", clazz, ex.getMessage()),
					ex);
		}
	}
}
