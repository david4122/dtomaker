package org.dtomaker;

public abstract class AbstractAccessorFactory {

	public abstract Getter createGetter(Class<?> clazz, String field);

	public abstract Setter createSetter(Class<?> clazz, String field);
}
