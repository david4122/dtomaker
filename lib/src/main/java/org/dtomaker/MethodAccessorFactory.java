package org.dtomaker;

import java.lang.reflect.Method;

public class MethodAccessorFactory extends AbstractAccessorFactory {

	@Override
	public Getter createGetter(Class<?> clazz, String field) {
		Method m = getMethod(clazz, getterName(field));
		return null == m ? null : new MethodGetter(m);
	}

	@Override
	public Setter createSetter(Class<?> clazz, String field) {
		Method m = getMethod(clazz, setterName(field));
		return null == m ? null : new MethodSetter(m);
	}

	private Method getMethod(Class<?> clazz, String name) {
		try {
			Method m = clazz.getDeclaredMethod(name);
			if(!m.trySetAccessible())
				return null;

			return m;

		} catch(NoSuchMethodException ex) {
			return null;
		}
	}


	private static String getterName(String field) {
		return accessorName("get", field);
	}

	private static String setterName(String field) {
		return accessorName("set", field);
	}

	private static String accessorName(String op, String field) {
		var accessorName = new StringBuilder(field.length() + op.length());

		accessorName.append(op)
			.append(field)
			.setCharAt(op.length(), Character.toUpperCase(field.charAt(0)));

		return accessorName.toString();
	}
}
