package org.dtomaker;

public interface Getter {

	<T> T get(Object source) throws IllegalAccessException;
}
