package org.dtomaker;

import java.lang.reflect.Method;

public class MethodSetter extends MethodAccessor implements Setter {

	public MethodSetter(Method m) {
		super(m);
	}

	@Override
	public void set(Object target, Object val) throws IllegalAccessException {
		invoke(target, val);
	}
}
