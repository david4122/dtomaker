package org.dtomaker;

import java.lang.reflect.Field;

public class DirectFieldAccessorFactory extends AbstractAccessorFactory {

	@Override
	public Getter createGetter(Class<?> clazz, String field) {
		Field f = getField(clazz, field);
		return null == f ? null : new DirectFieldGetter(f);
	}

	@Override
	public Setter createSetter(Class<?> clazz, String field) {
		Field f = getField(clazz, field);
		return null == f ? null : new DirectFieldSetter(f);
	}

	private Field getField(Class<?> clazz, String field) {
		try {
			Field f = clazz.getDeclaredField(field);
			if(!f.trySetAccessible())
				return null;

			return f;
		} catch(NoSuchFieldException ex) {
			return null;
		}
	}
}
