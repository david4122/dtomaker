package org.dtomaker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.UUID;

import org.dtomaker.annotation.From;
import org.dtomaker.annotation.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Entity {

	private Long id;

	private String str;

	private String noAccessors;

	public Entity(Long id, String str, String noAccessors) {
		this.id = id;
		this.str = str;
		this.noAccessors = noAccessors;
	}

	public String getDynamicValue() {
		return String.format("%d %s", id, str);
	}

	public Long getId() {
		return id;
	}

	public String getStr() {
		return str;
	}

	public boolean isHiddenEqualTo(String s) {
		return noAccessors.equals(s);
	}
}

class Dto {

	private Long id;

	private String str;

	private String noAccessors;

	private String dynamicValue;

	@Ignore
	private Object ignored = null;

	@From("str")
	private String changed;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	public String getNoAccessors() {
		return noAccessors;
	}

	public String getDynamicValue() {
		return dynamicValue;
	}

	public Object getIgnored() {
		return ignored;
	}

	public String getChanged() {
		return changed;
	}
}

public class DtoMakerTests {

	private DtoMaker dtoMaker;

	@BeforeEach
	void setup() {
		var factory = new AccessorFactoryChain();
		factory.registerAccessorFactory(new DirectFieldAccessorFactory());
		factory.registerAccessorFactory(new MethodAccessorFactory());

		this.dtoMaker = new DtoMaker(factory);
	}

	@Test
	void testDefault() {
		var entity = new Entity(randLong(), randStr(), randStr());

		var dto = dtoMaker.toDto(entity, Dto.class);

		assertNotNull(dto);
		assertEquals(entity.getId(), dto.getId());
		assertEquals(entity.getStr(), dto.getStr());
		assertTrue(entity.isHiddenEqualTo(dto.getNoAccessors()));
		assertEquals(entity.getDynamicValue(), dto.getDynamicValue());
		assertEquals(entity.getStr(), dto.getChanged());
	}


	private static Long randLong() {
		return (long) (Math.random() * 1000);
	}

	private static String randStr() {
		return UUID.randomUUID().toString();
	}
}
